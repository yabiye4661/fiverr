from importlib.metadata import files
import json
from flask import Flask, render_template, request, redirect, jsonify, send_from_directory
import pandas as pd
import gspread
import traceback
import re
import csv
import os.path
import io
from googleapiclient.http import MediaIoBaseDownload
from google.oauth2.service_account import Credentials as ServiceAccountCredentials
from googleapiclient.discovery import build
from googleapiclient.errors import HttpError

SCOPES = ['https://www.googleapis.com/auth/drive']
gc = None
app = Flask(__name__)
pattern = r'.*folders/(.*)(?:[/?]|$)'
drive_service = None
FOLDER_URL_REGEX = re.compile(pattern)
METADATA_FILE_NEW = 'files_metadata.new.csv'
METADATA_FILE_OLD = 'files_metadata.old.csv'
EXCEL_FILES_DIR = 'excel_files'
CREDS_PATH = 'creds.json'
DAYS = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

def get_metadata_map(df):
    mmap = {}
    for i, row in df.iterrows():
        mmap[row['Id']] = {
            "Name" : row['Name'],
            "Id"   : row['Id'],
            "ModifiedDate"  : row['ModifiedDate']
        }
    return mmap

def build_service_if_needed():
    global gc
    global drive_service
    if drive_service is None:
        if os.path.exists(CREDS_PATH):
            creds = ServiceAccountCredentials.from_service_account_file(
                CREDS_PATH, scopes=SCOPES)
        else:
            raise Exception("Credentials not found..")

        if gc is None:
            gc = gspread.service_account(filename=CREDS_PATH)

        drive_service = build('drive', 'v3', credentials=creds)

def get_list_of_excels(folder_id):
    creds = None
    global gc
    global drive_service
    build_service_if_needed()
    print("Folder id: ", folder_id)
    query = f"'{folder_id}' in parents and (mimeType = 'application/vnd.google-apps.spreadsheet' or mimeType = 'application/vnd.ms-excel' or mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')"
    fields="nextPageToken, files(id, name, modifiedTime)"
    print("Query: ", query)
    results = drive_service.files().list(
        pageSize=20,q=query, fields=fields).execute()
    items = results.get('files', [])

    if not items:
        print('No files found.')
        return
    name_to_id_map = {}
    with open(METADATA_FILE_NEW, "w") as f:
        csvw = csv.writer(f)
        csvw.writerow(["Name", "Id", "Modified Time"])
        for item in items:
            row = [item['name'].replace("/", "-"), item['id'], item['modifiedTime']]
            csvw.writerow(row)
            print(row)
            name_to_id_map[item['name'].replace("/", "-")] = item['id']
    return name_to_id_map

def drive_download(file_id, file_name):
    global drive_service
    build_service_if_needed()
    request = drive_service.files().get_media(fileId=file_id)
    file_name = file_name.replace("/", "-")
    file_path = f"{EXCEL_FILES_DIR}/{file_name}"
    print("Downloading file: ", file_name)
    fh = io.FileIO(file_path, mode='wb')
    try:
        downloader = MediaIoBaseDownload(fh, request, chunksize=1024*1024)
        done = False
        while done is False:
            status, done = downloader.next_chunk(num_retries = 2)
            if status:
                print("Download %d%%." % int(status.progress() * 100))
        print("Download complete")
    except:
        traceback.print_exc()
        fh.close()
        raise Exception("Download failed")
    finally:
        fh.close()

def drive_export(file_id, file_name):
    global drive_service
    build_service_if_needed()
    request = drive_service.files().export_media(fileId=file_id, mimeType='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    file_name = file_name.replace("/", "-")
    file_path = f"{EXCEL_FILES_DIR}/{file_name}.xlsx"
    print("Exporting file: ", file_name)
    fh = io.FileIO(file_path, mode='wb')
    try:
        downloader = MediaIoBaseDownload(fh, request, chunksize=1024*1024)
        done = False
        while done is False:
            status, done = downloader.next_chunk(num_retries = 2)
            if status:
                print("Export %d%%." % int(status.progress() * 100))
        print("Export complete")
    except:
        traceback.print_exc()
    finally:
        fh.close()

def create_output_csv_files(in_csv_but_not_excel, in_excel_but_not_csv, in_both, csv_custid_to_cust, excel_custid_to_cust):
    location_wise_csvs_map = {}
    for cid in in_both:
        custinfoincsv = csv_custid_to_cust[cid]['custinfo']
        custinfoinexcel = excel_custid_to_cust[cid]['custinfo']
        excelwise_customer_metadata = excel_custid_to_cust[cid]['excelwise_metadata']
        location = custinfoincsv['Location']
        if location not in location_wise_csvs_map:
            location_wise_csvs_map[location] = {
                "in_both" : [
                    ["Name", "Customer Id", "Location from csv", "sheet name of google sheet", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun", "amount"]
                ],
                "in_csv_only" : [
                    ["Name", "Customer Id", "Location from csv", "amount"]
                ],
                "in_excels_only" : [
                    ["Customer Id", "sheet name of google sheet", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]
                ]
            }
        for excelname, daywise_count_dict in excelwise_customer_metadata.items():
            daywise_count_dict = daywise_count_dict['daywise_count']
            location_wise_csvs_map[location]['in_both'].append([
                custinfoincsv['Customer'],
                cid,
                location,
                excelname,
                daywise_count_dict["Mon"],
                daywise_count_dict["Tue"],
                daywise_count_dict["Wed"],
                daywise_count_dict["Thu"],
                daywise_count_dict["Fri"],
                daywise_count_dict["Sat"],
                daywise_count_dict["Sun"],
                custinfoincsv['Amount']
            ])
    for cid in in_csv_but_not_excel:
        custinfoincsv = csv_custid_to_cust[cid]['custinfo']
        location = custinfoincsv['Location']
        if location not in location_wise_csvs_map:
            location_wise_csvs_map[location] = {
                "in_both" : [
                    ["Name", "Customer Id", "Location from csv", "sheet name of google sheet", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun", "amount"]
                ],
                "in_csv_only" : [
                    ["Name", "Customer Id", "Location from csv", "amount"]
                ],
                "in_excels_only" : [
                    ["Customer Id", "sheet name of google sheet", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]
                ]
            }
        
        location_wise_csvs_map[location]['in_csv_only'].append([
            custinfoincsv['Customer'],
            cid,
            location,
            custinfoincsv['Amount']
        ])
    for cid in in_excel_but_not_csv:
        csv_location_to_excel_location_map = {
            'McKinney' : 'McKinney',
            'Frisco' : 'Frisco',
            'Argyle' : 'Argyle',
            'Virtual (VLP)' : 'VLP',
            'Castle Hills/Lewisville' : 'CH',
            'North Richland Hills' : 'NorthRichlandHills',
            'Allen/Fairview' : 'Allen',
            'Southlake' : 'SL',
            'Coppell' : 'Coppell',
            'Plano' : 'Plano',
            'Grace Academy - Satellite': 'Grace Academy',
            'Denison (TexOma)':'Denison'
        }
        custinfoinexcel = excel_custid_to_cust[cid]['custinfo']
        excelwise_customer_metadata = excel_custid_to_cust[cid]['excelwise_metadata']
        for excelname, daywise_count_dict in excelwise_customer_metadata.items():
            location = None
            daywise_count_dict = daywise_count_dict['daywise_count']
            for csvloc, excelloc in csv_location_to_excel_location_map.items():
                if excelloc in excelname:
                    location = csvloc
                    break
            if location is None:
                print("Did not found a match for excel file with name ", excelname)
            else:
                if location not in location_wise_csvs_map:
                    location_wise_csvs_map[location] = {
                        "in_both" : [
                            ["Name", "Customer Id", "Location from csv", "sheet name of google sheet", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun", "amount"]
                        ],
                        "in_csv_only" : [
                            ["Name", "Customer Id", "Location from csv", "amount"]
                        ],
                        "in_excels_only" : [
                            ["Customer Id", "sheet name of google sheet", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat", "Sun"]
                        ]
                    }
                print(excelname, daywise_count_dict)
                location_wise_csvs_map[location]['in_excels_only'].append([
                    cid, excelname,
                    daywise_count_dict["Mon"],
                    daywise_count_dict["Tue"],
                    daywise_count_dict["Wed"],
                    daywise_count_dict["Thu"],
                    daywise_count_dict["Fri"],
                    daywise_count_dict["Sat"],
                    daywise_count_dict["Sun"]
                ])

    written_csv_files = []
    for f in os.listdir("generated_csvs"):
        os.remove(f"generated_csvs/{f}")
    for location, data_dict in location_wise_csvs_map.items():
        print(location)
        location = location.replace("/", "-")
        csv_file_name = f"generated_csvs/{location}.csv"
        with open(csv_file_name, "w") as f:
            csvw = csv.writer(f)
            csvw.writerow(["In both CSV and Excel:"])
            csvw.writerows(data_dict["in_both"])
            csvw.writerows([]*3)
            csvw.writerow(["Only in CSV:"])
            csvw.writerows(data_dict["in_csv_only"])
            csvw.writerows([]*3)
            csvw.writerow(["Only in Excel:"])
            csvw.writerows(data_dict["in_excels_only"])
        written_csv_files.append(csv_file_name)
    return written_csv_files

def customerdiff(csv_df, excels):
    csv_custid_to_cust = {}
    excel_custid_to_cust = {}
    for idx, row in csv_df.iterrows():
        custid = row['Customer QuickBooks Id']
        csv_custid_to_cust[custid] = {
            'custinfo' : row
        }
    EXCEL_CUST_ID_REGEX_PATTERN = r'.*\*(\d{3,5})$'
    EXCEL_CUST_ID_CELL_REGEX = re.compile(EXCEL_CUST_ID_REGEX_PATTERN)

    for eds in excels:
        excel_name = eds['name']
        dfs = eds['dfs']
        for day in DAYS:
            if dfs[day] is not None:
                df = dfs[day]
                for idx, row in df.iterrows():
                    rowlist = list(row)
                    for val in rowlist:
                        val = str(val)
                        # print(val)
                        match = EXCEL_CUST_ID_CELL_REGEX.search(val)
                        if not match or (len(re.findall(EXCEL_CUST_ID_REGEX_PATTERN, val)) != 1):
                            continue
                        custid = match.group(1)
                        if custid not in excel_custid_to_cust:
                            daywise_counts = {}
                            for dday in DAYS:
                                daywise_counts[dday] = 0
                            daywise_counts[day] = 1
                            excel_custid_to_cust[custid] = {
                                'custinfo' : row,
                                'excelwise_metadata' : {
                                    excel_name : {
                                        'daywise_count':daywise_counts
                                    }
                                }
                            }
                        elif excel_name not in excel_custid_to_cust[custid]['excelwise_metadata']:
                            daywise_counts = {}
                            for dday in DAYS:
                                daywise_counts[dday] = 0
                            daywise_counts[day] = 1
                            excel_custid_to_cust[custid]['excelwise_metadata'][excel_name] = {
                                'daywise_count':daywise_counts
                            }
                        else:
                            excel_custid_to_cust[custid]['excelwise_metadata'][excel_name]['daywise_count'][day] =\
                                excel_custid_to_cust[custid]['excelwise_metadata'][excel_name]['daywise_count'][day] + 1
    
    customers_in_csv = list(csv_custid_to_cust.keys())
    # print("CSV Customers: ", customers_in_csv)
    customers_in_excels = list(excel_custid_to_cust.keys())
    # print("Excel Customers: ", customers_in_excels)
    in_csv_but_not_excel = list(set(customers_in_csv) - set(customers_in_excels))
    in_excel_but_not_csv = list(set(customers_in_excels) - set(customers_in_csv))
    in_both = list(set(customers_in_csv) & set(customers_in_excels))
    print("Customer IDs in CSV but not excels: ", in_csv_but_not_excel)
    print("Customer IDs in excel but not CSV: ", in_excel_but_not_csv)
    return create_output_csv_files(in_csv_but_not_excel, in_excel_but_not_csv, in_both, csv_custid_to_cust, excel_custid_to_cust)


def compare(csv_df, excels_name_to_ids):
    excels = []
    
    files_metadata_df_new = pd.read_csv(METADATA_FILE_NEW)
    files_metadata_df_new['ModifiedDate'] = pd.to_datetime(files_metadata_df_new['Modified Time']).dt.tz_convert(None)
    files_metadata_df_old = None
    metadata_map_old = {}
    metadata_map_new = get_metadata_map(files_metadata_df_new)
    if os.path.exists(METADATA_FILE_OLD):
        files_metadata_df_old = pd.read_csv(METADATA_FILE_OLD)
        files_metadata_df_old['ModifiedDate'] = pd.to_datetime(files_metadata_df_old['Modified Time']).dt.tz_convert(None)
        metadata_map_old = get_metadata_map(files_metadata_df_old)
    for name, id in excels_name_to_ids.items():
        print(name, id)
        fpath = f"{EXCEL_FILES_DIR}/{name}"
        is_excel = fpath.endswith(".xlsx")
        if not fpath.endswith(".xlsx") and not fpath.endswith(".csv"):
            fpath = fpath + ".xlsx"
        if files_metadata_df_old is None or\
            id not in metadata_map_old or\
            metadata_map_old[id]['ModifiedDate'] < metadata_map_new[id]['ModifiedDate'] or\
            not os.path.exists(fpath):
            if is_excel:
                drive_download(id, name)
            else:
                drive_export(id, name)
        dfs = {}
        for day in DAYS:
            try:
                dfs[day] = pd.read_excel(fpath, sheet_name=day)
            except:
                dfs[day] = None
        excels.append({"name" : name, "dfs": dfs})
    files_metadata_df_new.to_csv(METADATA_FILE_OLD)
    return customerdiff(csv_df, excels)

@app.route('/', methods=["GET"])
def index_get():
    return render_template('index.html')

@app.route("/filesfromfolder", methods=["POST"])
def get_files_from_drive_folder_url():
    # files_list = {
    #     "foo.xlsx" : "foo",
    #     "bar.xlsx" : "bar",
    #     "lor.xlsx" : "lor",
    #     "baz.xlsx" : "baz"
    # }
    # return jsonify(files_list)
    if request.method == "POST":
        print(request.get_json())
        drive_folder_url = request.json["folder_url"]
        res = FOLDER_URL_REGEX.search(drive_folder_url)
        if not res or (len(re.findall(pattern, drive_folder_url)) != 1):
            raise Exception("Invalid format for google drive URL")
        drive_folder_id = res.group(1)
        excels_name_to_ids = get_list_of_excels(drive_folder_id)
    return jsonify(excels_name_to_ids)

@app.route("/savecreds", methods=["GET"])
def return_creds_file():
    return render_template("save_creds.html")

@app.route("/savecreds", methods=["POST"])
def save_creds():
    if request.method == "POST":
        if "file" not in request.files:
            return jsonify({"Error": "Seems like you forgot to attach the file!!"})
        file = request.files["file"]
        if file.filename == "" or not file.filename.endswith(".json"):
            return jsonify({"error" : "Invalid File Name"})
        if file:
            try:
                os.rename("creds.json", "creds.old.json")
                file.save("creds.json")
            except:
                os.rename("creds.old.json", "creds.json")
                return jsonify({"Error" : traceback.print_exc()})

    return jsonify({"Success": "File Saved Successfully!!"})


@app.route('/formsubmit', methods=["POST"])
def formsubmit():
    if request.method == "POST":
        if "file" not in request.files:
            return jsonify({"Error": "Seems like you forgot to attach the file!!"})
        file = request.files["file"]
        if file.filename == "":
            return redirect(request.url)
        selectedSheetsNameToIds = json.loads(request.form['selectedSheets'])
        if file:
            uploaded_csv_df = pd.read_csv(file, header=1)
            uploaded_csv_df = uploaded_csv_df.dropna(subset=['Location', 'Customer QuickBooks Id'])
            print("Uploaded csv df: ")
            print(uploaded_csv_df.head(2))
            written_csv_files = compare(uploaded_csv_df, selectedSheetsNameToIds)
            print("Created CSV Files")
            print(written_csv_files)
            return render_template("generated_csvs_download.html", files=os.listdir("generated_csvs"))


    return render_template('index.html')

@app.route("/howto")
def getinstructions():
    return render_template('howto.html')

@app.route('/generated_csvs/<path:filename>')
def generated_csv(filename):
    return send_from_directory(
        os.path.abspath('generated_csvs'),
        filename,
        as_attachment=True
    )

if __name__ == '__main__':
    app.run(debug=True, threaded=True)
